package rmqb

import (
	"errors"
	"fmt"
	"sync"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
)

var (
	ErrAlreadyClosed                   = errors.New("Already closed: not connected to the server")
	ErrBrokerPublisherTimelineExceeded = errors.New("Broker message publishing time limit exceeded")
)

type MLRabbitExchange struct {
	Name   string
	TypeEx string
}

// to emit messages. No queues.
type MLRabbit struct {
	Exchange        *MLRabbitExchange
	Queue           string
	Channel         *amqp.Channel
	NotifyChanClose chan *amqp.Error
	NotifyConfirm   chan amqp.Confirmation
	RKey            string
}

type RmqSession struct {
	m                sync.Mutex
	connectionE      *amqp.Connection
	DoneE            chan bool
	notifyConnEClose chan *amqp.Error
	Emitter          *MLRabbit
	IsReadyE         bool
	connectionC      *amqp.Connection
	DoneC            chan bool
	notifyConnCClose chan *amqp.Error
	Consumer         *MLRabbit
	IsReadyC         bool
}

func (s *RmqSession) MakeSession(
	emitterStartChan, consumerStartChan chan struct{},
	useSsl bool,
	rmqInit string,
	emitterExchange, consumerExchange MLRabbitExchange,
	emitterRoutingKey, consumerRoutingKey string,
	reconnectDelay, reInitDelay time.Duration,
) error {
	var (
		addr string
		err  error
	)
	s.DoneE = make(chan bool)
	s.DoneC = make(chan bool)
	s.Emitter = &MLRabbit{Exchange: &emitterExchange, RKey: emitterRoutingKey}
	s.Consumer = &MLRabbit{Exchange: &consumerExchange, RKey: consumerRoutingKey}

	if err != nil {
		return err
	}
	if useSsl {
		addr = fmt.Sprintf("%s%s", "amqps://", rmqInit)
	} else {
		addr = fmt.Sprintf("%s%s", "amqp://", rmqInit)
	}
	go s.reconnectE(emitterStartChan, addr, reconnectDelay, reInitDelay)
	go s.reconnectC(consumerStartChan, addr, reconnectDelay, reInitDelay)
	return nil
}

func (s *RmqSession) reconnectE(
	emitterStartChan chan struct{},
	addr string, reconnectDelay, reInitDelay time.Duration) {
	for {
		s.m.Lock()
		s.IsReadyE = false
		s.m.Unlock()
		wsugar.Infow("Attempting to connect emitter")
		conn, err := amqp.Dial(addr)
		// wsugar.Infow(fmt.Sprintf("err: %v\n", err))
		if err != nil {
			wsugar.Errorw("Failed to connect. Retrying emitter...")
			select {
			case <-s.DoneE:
				return
			case <-time.After(reconnectDelay):
			}
			continue
		}
		s.m.Lock()
		s.connectionE = conn
		s.notifyConnEClose = make(chan *amqp.Error, 1)
		s.connectionE.NotifyClose(s.notifyConnEClose)
		wsugar.Infow("Connected emitter!")
		s.m.Unlock()
		if done := s.handleReInitE(emitterStartChan, conn, reInitDelay); done {
			break
		}
	}
}

func (s *RmqSession) reconnectC(
	consumerStartChan chan struct{},
	addr string, reconnectDelay, reInitDelay time.Duration) {
	for {
		s.m.Lock()
		s.IsReadyC = false
		s.m.Unlock()
		wsugar.Infow("Attempting to connect consumer")
		conn, err := amqp.Dial(addr)
		// wsugar.Infow(fmt.Sprintf("err: %v\n", err))
		if err != nil {
			wsugar.Errorw("Failed to connect. Retrying consumer...")
			select {
			case <-s.DoneC:
				return
			case <-time.After(reconnectDelay):
			}
			continue
		}
		s.m.Lock()
		s.connectionC = conn
		s.notifyConnCClose = make(chan *amqp.Error, 1)
		s.connectionC.NotifyClose(s.notifyConnCClose)
		s.m.Unlock()
		wsugar.Infow("Connected consumer!")
		if done := s.handleReInitC(consumerStartChan, conn, reInitDelay); done {
			break
		}
	}
}

func (s *RmqSession) handleReInitE(
	emitterStartChan chan<- struct{},
	conn *amqp.Connection,
	reInitDelay time.Duration,
) bool {
	for {
		s.m.Lock()
		s.IsReadyE = false
		s.m.Unlock()
		err := s.initEmitter(conn)
		if err != nil {
			wsugar.Errorw("Failed to initialize emitter channel. Retrying emitter...")
			select {
			case <-s.DoneE:
				return true
			case <-time.After(reInitDelay):
			}
			continue
		}
		// send message to SERVER:
		emitterStartChan <- struct{}{}
		// go s.PublishMessage(SendMLProcessedMessageChan, MLErrors, TimeoutSendMessage)

		select {
		case <-s.DoneE:
			return true
		case <-s.notifyConnEClose:
			wsugar.Infow("Connection emitter closed. Reconnecting emitter...")
			return false
		case <-s.Emitter.NotifyChanClose:
			wsugar.Infow("Emitting channel closed. Re-running emitter init...")
			s.m.Lock()
			s.IsReadyE = false
			s.m.Unlock()
		}
	}
}

func (s *RmqSession) handleReInitC(
	consumerStartChan chan<- struct{},
	conn *amqp.Connection,
	reInitDelay time.Duration,
) bool {
	for {
		s.m.Lock()
		s.IsReadyC = false
		s.m.Unlock()
		err := s.initConsumer(conn)
		if err != nil {
			wsugar.Errorw("Failed to initialize consumer channel. Retrying consumer...")
			select {
			case <-s.DoneC:
				return true
			case <-time.After(reInitDelay):
			}
			continue
		}
		// get message from SERVER:
		consumerStartChan <- struct{}{}
		// go s.SubscribeMessage(RecieveMessageForMLProcessingChan, MLErrors)

		select {
		case <-s.DoneC:
			return true
		case <-s.notifyConnCClose:
			wsugar.Infow("Connection consumer closed. Reconnecting consumer...")
			return false
		case <-s.Consumer.NotifyChanClose:
			wsugar.Infow("Consuming channel closed. Re-running consumer init...")
			s.m.Lock()
			s.IsReadyC = false
			s.m.Unlock()
		}
	}
}

func (s *RmqSession) initEmitter(conn *amqp.Connection) error {
	var (
		err error
	)
	// Emitter:
	ch, err := conn.Channel()
	if err != nil {
		return err
	}
	// create exchanges:
	wsugar.Info("start creating Emitter exchange")
	err = ch.ExchangeDeclare(
		s.Emitter.Exchange.Name,   // name
		s.Emitter.Exchange.TypeEx, // type
		true,                      // durable
		false,                     // auto-deleted
		false,                     // internal
		false,                     // no-wait
		nil,                       // arguments
	)
	if err != nil {
		return err
	}
	err = ch.Confirm(false)
	if err != nil {
		return err
	}
	s.m.Lock()
	s.Emitter.Channel = ch
	s.Emitter.NotifyChanClose = make(chan *amqp.Error, 1)
	s.Emitter.NotifyConfirm = make(chan amqp.Confirmation, 1)
	s.Emitter.Channel.NotifyClose(s.Emitter.NotifyChanClose)
	s.Emitter.Channel.NotifyPublish(s.Emitter.NotifyConfirm)
	s.IsReadyE = true
	s.m.Unlock()
	wsugar.Infow("Setup emitter!")

	return nil
}

// Consumer:
func (s *RmqSession) initConsumer(conn *amqp.Connection) error {
	var (
		err error
	)
	ch, err := conn.Channel()
	if err != nil {
		return err
	}
	// create exchanges:
	wsugar.Info("start creating Consumer exchange")
	err = ch.ExchangeDeclare(
		s.Consumer.Exchange.Name,   // name
		s.Consumer.Exchange.TypeEx, // type
		true,                       // durable
		false,                      // auto-deleted
		false,                      // internal
		false,                      // no-wait
		nil,                        // arguments
	)
	if err != nil {
		return err
	}
	err = ch.Confirm(false)
	if err != nil {
		return err
	}
	s.m.Lock()
	wsugar.Info(fmt.Sprintf("create queues for exchange : %s with routing key: %s\n",
		s.Consumer.Exchange.Name, s.Consumer.RKey))
	s.m.Unlock()
	q, err := ch.QueueDeclare(
		"",    // name
		false, // durable
		false, // delete when unused
		true,  // exclusive
		false, // no-wait
		nil,   // arguments
	)
	if err != nil {
		return err
	}
	s.m.Lock()
	s.Consumer.Queue = q.Name
	// bind queue:
	wsugar.Info(fmt.Sprintf("bind queue %s to exchange %s with routing key: %s\n",
		q.Name, s.Consumer.Exchange.Name, s.Consumer.RKey))
	s.m.Unlock()
	err = ch.QueueBind(
		q.Name,                   // queue name
		s.Consumer.RKey,          // routing key
		s.Consumer.Exchange.Name, // exchange
		false,
		nil)
	if err != nil {
		return err
	}
	s.m.Lock()
	s.Consumer.Channel = ch
	s.Consumer.NotifyChanClose = make(chan *amqp.Error, 1)
	s.Consumer.NotifyConfirm = make(chan amqp.Confirmation, 1)
	s.Consumer.Channel.NotifyClose(s.Consumer.NotifyChanClose)
	s.Consumer.Channel.NotifyPublish(s.Consumer.NotifyConfirm)
	s.IsReadyC = true
	s.m.Unlock()

	wsugar.Infow("Setup consumer!")
	return nil
}

func (s *RmqSession) Close() error {
	if !s.IsReadyE {
		return ErrAlreadyClosed
	}
	close(s.DoneE)
	s.IsReadyE = false

	if !s.IsReadyC {
		return ErrAlreadyClosed
	}
	close(s.DoneC)
	s.IsReadyC = false
	return nil
}
