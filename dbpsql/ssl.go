package dbpsql

import (
	"errors"
	"fmt"
	"strings"
)

const (
	sslEnvRequired    = "POSTGRESQL_SSL_REQUIRED"
	sslEnvModeType    = "POSTGRESQL_SSLMODE_TYPE"
	sslEnvKey         = "sslkey"
	sslEnvCert        = "sslcert"
	sslEnvRootCert    = "sslrootcert"
	sslModeRequire    = "require"
	sslModeVerifyCa   = "verify-ca"
	sslModeVerifyFull = "verify-full"
	sslModeDisable    = "disable"
)

var (
	modeTypeSSLError = errors.New(fmt.Sprintf("dsn string, must be contains %s, %s, %s",
		sslEnvKey, sslEnvCert, sslEnvRootCert))
	sslRequiredError = errors.New(fmt.Sprintf("environment \"%s\" must be one of value: %s, %s, %s",
		sslEnvModeType, sslModeRequire, sslModeVerifyCa, sslModeVerifyFull))
)

func checkDsnSSL(dsn string) error {
	if !strings.Contains(dsn, sslEnvKey) && !strings.Contains(dsn, sslEnvCert) && !strings.Contains(dsn, sslEnvRootCert) {
		return modeTypeSSLError
	}
	return nil
}
