package dbpsql

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"os"
)

func NewPsqlDb(dsn string) (*sql.DB, error) {
	dsn, err := prepareDSN(dsn)
	if err != nil {
		return nil, fmt.Errorf("failed to connect to database: %s", err.Error())
	}

	c, err := sql.Open("postgres", dsn)
	if err != nil {
		return nil, fmt.Errorf("failed to connect to database: %s", err.Error())
	}

	err = c.Ping()
	if err != nil {
		return nil, fmt.Errorf("failed to ping database: %s", err.Error())
	}

	return c, nil
}

func prepareDSN(dsn string) (string, error) {
	defaultTo := func(env string, value string) error {
		if os.Getenv(env) == "" {
			err := os.Setenv(env, value)
			if err != nil {
				return fmt.Errorf("SSL connect error: %s", err.Error())
			}
		}
		return nil
	}

	if err := defaultTo(sslEnvRequired, "false"); err != nil {
		return "", err
	}

	if err := defaultTo(sslEnvModeType, sslModeDisable); err != nil {
		return "", err
	}

	sslRequired := os.Getenv(sslEnvRequired)
	envSslModeType := os.Getenv(sslEnvModeType)
	if sslRequired == "true" && envSslModeType == sslModeDisable {
		return "", fmt.Errorf("SSL connect error: %s", sslRequiredError.Error())
	}

	if sslRequired == "true" && (envSslModeType == sslModeVerifyCa || envSslModeType == sslModeVerifyFull) {
		err := checkDsnSSL(dsn)
		if err != nil {
			return "", fmt.Errorf("SSL connect error: %s", err.Error())
		}
	}

	return fmt.Sprintf("%v sslmode=%s", dsn, envSslModeType), nil
}

func GetVersion(db *sql.DB) string {
	var version string
	err := db.QueryRow("select version()").Scan(&version)
	if err != nil {
		return "unknown"
	}
	return version
}
