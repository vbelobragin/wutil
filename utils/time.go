package utils

import (
	"fmt"
	"strings"
	"time"
)

const (
	TimeFormat         = "2006-01-02T15:04:05-07:00"
	TimeFormatContext  = "2006-01-02T15:04:05"
	TimeFormatContext1 = "2006-01-02T15:04:05Z"
	TimeFormatContext2 = "2006-01-02T15:04:05 MSK"
)

type JsonTime struct {
	time.Time
}

func (jt *JsonTime) UnmarshalJSON(b []byte) (err error) {
	s := strings.Trim(string(b), `"`)
	if s == "null" || s == "" {
		jt.Time = time.Time{}
		return
	}
	jt.Time, err = time.Parse(TimeFormat, s)
	return
}

func (jt JsonTime) MarshalJSON() ([]byte, error) {
	if jt.Time.IsZero() {
		return []byte(`""`), nil
	}
	return []byte(fmt.Sprintf(`"%s"`, jt.Time.Format(TimeFormat))), nil
}
