package utils

import "github.com/google/uuid"

type UUIDGenerator interface {
	Get() string
}

type uuidGenerator struct {
}

func (u uuidGenerator) Get() string {
	return uuid.NewString()
}

func NewUUIDGenerator() UUIDGenerator {
	return uuidGenerator{}
}
