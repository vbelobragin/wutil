package utils

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
)

func HashFileName(fileName, userID string) string {
	fe := GetFileExtension(fileName)
	hash := md5.Sum([]byte(fmt.Sprintf("%s%s", userID, fileName)))
	return fmt.Sprintf("%s.%s", hex.EncodeToString(hash[:]), fe)
}
