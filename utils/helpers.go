package utils

import (
	"errors"
	"fmt"
	"regexp"
	"sort"
	"strings"

	"reflect"

	"gitlab.com/vbelobragin/wutil/wfutil"
)

var (
	// introduce logger:
	wsugar = wfutil.LogInit(true, true, "")
)

// provide file extension:
func GetFileExtension(fileName string) string {
	temp := strings.Split(fileName, ".")
	return temp[len(temp)-1]
}

/*
	 validate integer:
		if input is not integer - output error,
		otherwise output nil error
*/
func ValidateInt(n interface{}) error {
	if _, ok := n.(int); ok {
		return nil
	}
	temp := fmt.Sprintf("%v is not integer\n", n)
	// wsugar.Errorw(temp)
	return errors.New(temp)
}

// general purpose foo allows to check if an element (string) is in a slice
func Contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

// select unique uin8 values
func UniqueU8(mySlice []uint8) []uint8 {
	keys := make(map[uint8]bool)
	list := []uint8{}
	for _, entry := range mySlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

func ValidateRegKey(key, pattern string) bool {
	re := regexp.MustCompile(pattern)
	// fmt.Println(re.FindStringIndex(key))
	return re.MatchString(key)
}

// transform map keys to slice and sort somehow
// func TransformMapKeysToSlice(inpMap map[string]interface{}) ([]string) {
func TransformMapKeysToSlice[T comparable](inpMap map[string]T) []string {
	temp := make([]string, 0, len(inpMap))
	for j := range inpMap {
		temp = append(temp, j)
	}
	sort.Slice(temp, func(i, j int) bool {
		return temp[i] > temp[j]
	})
	return temp
}

func CopyCommonFields(destp, srcp interface{}) error {
	destv := reflect.ValueOf(destp).Elem()
	srcv := reflect.ValueOf(srcp).Elem()
	destt := destv.Type()
	for i := 0; i < destt.NumField(); i++ {
		sf := destt.Field(i)
		// wsugar.Infow(fmt.Sprintf("OTLADKA: sf: %+v", sf))
		v := srcv.FieldByName(sf.Name)
		// wsugar.Infow(fmt.Sprintf("OTLADKA: v: %+v", v))
		if !v.IsValid() || !v.Type().AssignableTo(sf.Type) {
			continue
		}
		destv.Field(i).Set(v)
	}
	return nil
}

func ValidateSortOrder(sortOrder string) bool {
	return Contains([]string{"ASC", "DESC", "asc", "desc"}, sortOrder)
}

func ValidateSortBy(sortBy string, sortRange []string) bool {
	return Contains(sortRange, sortBy)
}

func AnyRegToFirstCapitilized(inp interface{}) string {
	switch inp.(type) {
	case string:
		s := inp.(string)
		return fmt.Sprintf("%s%s",
			strings.ToUpper(string([]rune(s)[0])), strings.ToLower(string([]rune(s)[1:])))
	default:
		return ""
	}
}
