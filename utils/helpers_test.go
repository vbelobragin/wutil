package utils

import (
	"fmt"
	"reflect"
	"testing"
)

var e error

func Test_ValidateInt(t *testing.T) {
	if e = ValidateInt(765); e != nil {
		t.Fatalf(`Incorrect ValidateInt: WANT: nil, GET: %s`, e)
	}
	if e = ValidateInt("765"); e == nil {
		t.Fatalf(`Incorrect ValidateInt: WANT: %s, GET: 'nil'`, e)
	}
	if e = ValidateInt("barsuk"); e == nil {
		t.Fatalf(`Incorrect ValidateInt: WANT: %s, GET: 'nil'`, e)
	}
	if e = ValidateInt(3.14); e == nil {
		t.Fatalf(`Incorrect ValidateInt: WANT: %s, GET: 'nil'`, e)
	}
	p := 6
	pp := &p
	if e = ValidateInt(*pp); e != nil {
		t.Fatalf(`Incorrect ValidateInt: WANT: %s, GET: 'nil'`, e)
	}
	z := "mypig"
	zz := &z
	if e = ValidateInt(*zz); e == nil {
		t.Fatalf(`Incorrect ValidateInt: WANT: %s, GET: 'nil'`, e)
	}
}

func Test_ValidateUniqueU8(t *testing.T) {
	a := []uint8{1, 5, 3, 6, 9, 9, 4, 2, 3, 1, 5}
	b := []uint8{1, 5, 3, 6, 9, 4, 2}
	ee := UniqueU8(a)

	if !reflect.DeepEqual(ee, b) {
		t.Fatalf(`Incorrect UniqueU8: WANT: %v, GET: %v`, b, ee)
	}
}

func Test_ValidateRegKey(t *testing.T) {
	key := "ggg"
	fmt.Println(len(key))
	pattern := "^[a-z][a-z0-9_]+$"
	ValidateRegKey(key, pattern)
}

func Test_AnyRegToFirstCapitilized(t *testing.T) {
	const output = "Diagnose"
	var y string
	t.Run("all lower", func(t *testing.T) {
		if y = AnyRegToFirstCapitilized("diagnose"); y != output {
			t.Fatalf("Incorrect ValidateInt: WANT: 'Diagnose', GET: %s\n", y)
		}
	})
	t.Run("all upper", func(t *testing.T) {
		if y = AnyRegToFirstCapitilized("DIAGNOSE"); y != output {
			t.Fatalf("Incorrect ValidateInt: WANT: 'Diagnose', GET: %s\n", y)
		}
	})
	t.Run("mix-1", func(t *testing.T) {
		if y = AnyRegToFirstCapitilized("DiAGnOse"); y != output {
			t.Fatalf("Incorrect ValidateInt: WANT: 'Diagnose', GET: %s\n", y)
		}
	})
	t.Run("mix-2", func(t *testing.T) {
		if y = AnyRegToFirstCapitilized("diagNOSE"); y != output {
			t.Fatalf("Incorrect ValidateInt: WANT: 'Diagnose', GET: %s\n", y)
		}
	})
	t.Run("mix-3", func(t *testing.T) {
		if y = AnyRegToFirstCapitilized("dIAGNOSE"); y != output {
			t.Fatalf("Incorrect ValidateInt: WANT: 'Diagnose', GET: %s\n", y)
		}
	})
	t.Run("mix-4", func(t *testing.T) {
		if y = AnyRegToFirstCapitilized("Diagnose"); y != output {
			t.Fatalf("Incorrect ValidateInt: WANT: 'Diagnose', GET: %s\n", y)
		}
	})
}
