package wfutil

import (
	"fmt"
	"io"
	"net/url"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

const (
	CustomOutO    = "CustomOutO"
	customSuffixO = "anyone"
)

type customWriter struct {
	io.Writer
}

func (cw customWriter) Close() error {
	// modify as preferred, for testing purpose this is enough:
	return nil
}
func (cw customWriter) Sync() error {
	// modify as preferred, for testing purpose this is enough:
	return nil
}

/*
TODO:
если метод глобальный, то правильно пользоваться логирование аля json,
например {"component": "cron", "message": "failed to renew vancancies",
"details": "somedetail", "ids", "123,456,758", "somecode": "somevalue"}
таким образом получится исползовать встроенный json процессор в графане
для поиска логов по ключам {app=someapp} | json | component="cron"
*/

/*
Attn!
 1. if use stdOut, can leave outpathO blank: `""` (and leave `buf“ nil)
 2. for any case of io.Writer output just set outpathO to CustomOutO accordingly.
    Of cource, you can change suffix above for the scheme as you like.
*/
func StructuredInit(y string, outpathO string, buf io.Writer) *zap.Logger {
	if outpathO == CustomOutO {
		err := zap.RegisterSink(outpathO, func(u *url.URL) (zap.Sink, error) {
			return customWriter{buf}, nil
		})
		if err != nil {
			panic(err)
		}
		outpathO = fmt.Sprintf("%s:%s", outpathO, customSuffixO)
	} else {
		outpathO = "stdout"
	}

	var logCfg = zap.Config{
		Encoding:         "json",
		OutputPaths:      []string{outpathO},
		ErrorOutputPaths: []string{"stdout"},
		EncoderConfig: zapcore.EncoderConfig{
			MessageKey:  "message",
			LevelKey:    "level",
			EncodeLevel: zapcore.LowercaseLevelEncoder,

			FunctionKey: "function",

			TimeKey:    "time",
			EncodeTime: zapcore.ISO8601TimeEncoder,

			CallerKey:    "caller",
			EncodeCaller: zapcore.ShortCallerEncoder,
		},
	}
	level := zapcore.InfoLevel
	switch y {
	case "DEBUG":
		level = zapcore.DebugLevel
	case "WARN":
		level = zapcore.WarnLevel
	case "ERROR":
		level = zapcore.ErrorLevel
	case "PANIC":
		level = zapcore.PanicLevel
	case "DPANIC":
		level = zapcore.DPanicLevel
	case "FATAL":
		level = zapcore.FatalLevel
	default:
	}
	logCfg.Level = zap.NewAtomicLevelAt(level)
	logCfg.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	return zap.Must(logCfg.Build())
}
