package wfutil

import (
	"os"
	"io"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

type WriteSyncer struct {
    io.Writer
}

func (ws WriteSyncer) Sync() error {
    return nil
}

// create log file:
func TouchFile(name string) error {
    file, err := os.OpenFile(name, os.O_RDONLY|os.O_CREATE, 0644)
    if err != nil {
        return err
    }
    return file.Close()
}

// func logInit(d bool, f *os.File) *zap.SugaredLogger {
func LogInit(d bool, filelogger bool, f string) *zap.SugaredLogger {

    pe := zap.NewProductionEncoderConfig()
	pe.EncodeTime = zapcore.ISO8601TimeEncoder
	pe.TimeKey = "timestamp"
	pe.LevelKey = "level"
	pe.NameKey = "name"
	pe.MessageKey = "msg"
	pe.CallerKey = "caller"
	// pe.StacktraceKey = "stacktrace"
	// do we really need this? 
	// https://stackoverflow.com/questions/3988788/what-is-a-stack-trace-and-how-can-i-use-it-to-debug-my-application-errors

    consoleEncoder := zapcore.NewConsoleEncoder(pe)

    level := zap.InfoLevel
    if d {
        level = zap.DebugLevel
    }

	core := zapcore.NewTee(
		zapcore.NewCore(consoleEncoder, zapcore.AddSync(os.Stdout), level),
	)
    // use file logger ONLY on debugging (on local device):
    if filelogger {
		fileEncoder := zapcore.NewJSONEncoder(pe)
		core = zapcore.NewTee(
			zapcore.NewCore(fileEncoder, getWriteSyncer(f), level),
			zapcore.NewCore(consoleEncoder, zapcore.AddSync(os.Stdout), level),
		)
	}

    l := zap.New(core, zap.AddCaller())

    return l.Sugar()
}

func getWriteSyncer(logName string) zapcore.WriteSyncer {
    var ioWriter = &lumberjack.Logger{
        Filename:   logName,
        MaxSize:    1, // MB
        MaxBackups: 3,  // number of backups
        MaxAge:     2, //days
        LocalTime:  true,
        Compress:   false, // disabled by default
    }
    var sw = WriteSyncer{
        ioWriter,
    }
    return sw
}

var (
	wlogger *zap.Logger
)