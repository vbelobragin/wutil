module gitlab.com/vbelobragin/wutil

go 1.20

require (
	github.com/google/uuid v1.3.0
	github.com/lib/pq v1.10.7
	github.com/rabbitmq/amqp091-go v1.8.1
	go.uber.org/zap v1.24.0
	gopkg.in/natefinch/lumberjack.v2 v2.2.1
)

require (
	github.com/pkg/errors v0.9.1 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
)
